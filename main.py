#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This is main executable python file"""
from subprocess import call
from PySide import QtCore, QtGui, QtSql
import json, logging, sys


class FileNotFoundError(Exception):
  pass


## LOAD SETTINGS
# ----------------------------------
"""central settings object"""

Settings = {}

def validate():
  """Throws KeyError if Settings object not valid"""
  global Settings
  Settings["dbSettings"]
  Settings["language"]

def loadSettings():
  """Load the Settings and validate them"""
  global Settings
  try:
    """Load the Settings from settings.json"""
    data = open("settings.json").read()
    Settings = json.loads(data)
    validate()
  except (FileNotFoundError, KeyError) as e:
    """Load the Settings from settings_default.json"""
    data = open("settings_default.json").read()
    Settings = json.loads(data)
    try:
      validate()
    except KeyError as e:
      logging.error("""settings.json and settings_default.json files are
        both corrupt! Please reinstall the program!""")
      sys.exit(1)





## CREATE PYTHON EXECUTABLES FROM ALL QT FILES
# ----------------------------------
# transform .ui into .py
for name in ["MainWindow", "DBSettings", "About", "License"]:
  call(("pyside-uic -i2 -x %s.ui --output=%s.py" %(name,name) ).split())

# transform .qrc into .py
for name in ["resource"]:
  call(("pyside-rcc %s.qrc -o %s_rc.py -py3 -compress 9" % (name,name)).split())





## IMPORT WINDOWS CLASSES
# ----------------------------------
# import the just regenerated windows
from MainWindow import Ui_MainWindow as MainWindowRaw
from DBSettings import Ui_Dialog as DBSettingsRaw
from About import Ui_Dialog as AboutRaw
from License import Ui_Dialog as LicenseRaw


# wrap them into working classes
class About(AboutRaw, QtGui.QDialog):
  """Class wrapping About dialog"""

  def __init__(self, *args, **kwargs):
    """Wrap around the About dialog class and adding functionality"""
    super(About, self).__init__(*args, **kwargs)
    
    # load elements from About.ui / .py
    self.setupUi(self)


class License(LicenseRaw, QtGui.QDialog):
  """Class wrapping License dialog"""

  def __init__(self, *args, **kwargs):
    """Wrap around the License dialog class and adding functionality"""
    super(License, self).__init__(*args, **kwargs)
    
    # load elements from License.ui / .py
    self.setupUi(self)


class DBSettings(DBSettingsRaw, QtGui.QDialog):
  """Class wrapping DBSettings dialog and adding functionality"""

  def __init__(self, *args, **kwargs):
    """Initializer for Dialog showing DBSettings"""
    super(DBSettings, self).__init__(*args, **kwargs)
    
    # set as modal (=steals focus and disable MainWindow to work)
    self.setModal(True)
    # load elements from DBSettings.ui / .py
    self.setupUi(self)
    # refresh DB types in combo box
    self.dbTypesCombo.clear()
    availableDbTypes = QtSql.QSqlDatabase.drivers() 
    self.dbTypesCombo.addItems( availableDbTypes)
    
    # set the options data into the dialog
    global Settings
    self.dbSettings = Settings["dbSettings"]
    self.hostEdit.setText( self.dbSettings.get("dbHost",""))
    self.dbNameEdit.setText( self.dbSettings.get("dbName",""))
    self.userEdit.setText( self.dbSettings.get("dbUser",""))
    self.passwordEdit.setText( self.dbSettings.get("dbPassword",""))
    self.cOptionsEdit.setText( self.dbSettings.get("dbCOptions",""))
    self.portEdit.setValue( -1)
    try:
      self.portEdit.setValue( int(self.dbSettings["dbPort"]))
    except:
      logging.warning("Invalid port number in config given!")
    try:
      ind = availableDbTypes.index( self.dbSettings["dbType"])
      self.dbTypesCombo.setCurrentIndex( ind)
    except:
      logging.warning("Bad 'dbType' in config given!")
    
  def accept(self, *args, **kwargs):
    """Action called when OK button clicked
    Write into central Settings."""
    self.dbSettings["dbType"] = self.dbTypesCombo.currentText()
    self.dbSettings["dbHost"] = self.hostEdit.text()
    self.dbSettings["dbName"] = self.dbNameEdit.text()
    self.dbSettings["dbUser"] = self.userEdit.text()
    self.dbSettings["dbPassword"] = self.passwordEdit.text()
    self.dbSettings["dbPort"] = self.portEdit.value()
    self.dbSettings["dbCOptions"] = self.cOptionsEdit.text()
    super().accept(*args, **kwargs)


class MainWindow(MainWindowRaw, QtGui.QMainWindow):
  """Class wrapping MainWindow + adding the functionality"""

  def __init__(self, *args, **kwargs):
    """Wrap around the MainWindow window class"""
    super(MainWindow, self).__init__(*args, **kwargs)
    
    # load elements from MainWindow.ui / .py
    self.setupUi(self)
    
    # bind the events 
    self.actionAbout_the_program.triggered.connect( self.callAboutTheProgram)
    self.actionLicense.triggered.connect( self.callLicense)
    self.actionDB_settings.triggered.connect( self.callDBSettings)
    self.actionReload.triggered.connect( self.reloadData)
    self.actionConnect.triggered.connect( self.connectToDB)
    self.actionDisconnect.triggered.connect( self.disconnectDB)

    self.deviceView.clicked.connect( self.showDeviceDetails)
    self.deviceView.clicked.connect( self.showReservationDetails)
    
    # TODO: implement all events
    #self.actionCheck_for_updates.triggered.connect( )
    #self.actionLanguage.triggered.connect( )
    #self.actionGive_feedback.triggered.connect( )
    
    # set local variables
    global Settings
    self.Settings = Settings
    self.connection = None
    # connect to the DB
    if not self.connectToDB( force=True):
      QtGui.QMessageBox.about(self, "Bad DB connection",
        "Please, set the correct DB connection.\n\nActual connection error:\n" +
        str(self.connection.lastError()))
      self.callDBSettings()
    
  def callAboutTheProgram(self):
    """Shows modal About dialog"""
    aboutTheProgram = About()
    aboutTheProgram.exec_()
  
  def callLicense(self):
    """Shows modal License dialog"""
    license = License()
    license.exec_()
  
  def callDBSettings(self):
    """Shows modal DBSettings dialog and queries user for DB connection info"""
    dbSettings = DBSettings()
    if dbSettings.exec_() != QtGui.QDialog.Accepted:
      # user changes dropped
      return
    
    # user changes to be tested:
    #reconnect the DB
    if not self.connectToDB():
      logging.error("When changing DB settings, error occured:",
        self.connection.lastError() )
      QtGui.QMessageBox.about(self,"DB settings error",
        "The DB settings was BAD or NOT WRITTEN to the file\n\nDetails:\n" +
        str( self.connection.lastError()))
    print("tables: ", self.connection.tables())
    
  def writeSettings(self):
    logging.warning("writeSettings")
    with open("settings.json","w") as fOut:
      fOut.write( json.dumps( self.Settings))
  
  def connectToDB(self, force=False):
    """Use Settings data to connect to the DB"""
    logging.warning("connectToDB")
    dbSettings = self.Settings["dbSettings"]
    db = QtSql.QSqlDatabase.addDatabase( dbSettings["dbType"])
    db.setDatabaseName( dbSettings.get("dbName", ""))
    db.setHostName( dbSettings.get("dbHost", ""))
    db.setUserName( dbSettings.get("dbUser", ""))
    db.setPassword( dbSettings.get("dbPassword", ""))
    db.setPort( int(dbSettings.get("dbPort", -1)))
    db.setConnectOptions( dbSettings.get("dbCOptions", ""))
    logging.debug("settings: ", db.hostName(), db.databaseName(),
      db.userName(), db.password(), db.port(), db.connectOptions())
    
    ret = db.open()
    if ret or force: # set new connection only if valid or first set
      self.connection = db
      # save correct settings
      self.writeSettings()
      self.reloadData()
    return ret

  def disconnectDB(self):
    self.connection.close()

  def reloadData(self):
    """Reloads the data from DB"""
    # set device model structure
    dm = QtSql.QSqlTableModel()
    dm.setTable("devices");
    dm.setEditStrategy( QtSql.QSqlTableModel.EditStrategy.OnManualSubmit);
    dm.select();
    
    # set the names of columns
    dm.setHeaderData(0, QtCore.Qt.Horizontal, "id");
    dm.setHeaderData(1, QtCore.Qt.Horizontal, "reservable", QtCore.Qt.DisplayRole);
    dm.setHeaderData(2, QtCore.Qt.Horizontal, "serialnum", QtCore.Qt.DisplayRole);
    dm.setHeaderData(3, QtCore.Qt.Horizontal, "invnum", QtCore.Qt.DisplayRole);
    dm.setHeaderData(4, QtCore.Qt.Horizontal, "location_id", QtCore.Qt.DisplayRole);
    dm.setHeaderData(5, QtCore.Qt.Horizontal, "workplace_number", QtCore.Qt.DisplayRole);
    dm.setHeaderData(6, QtCore.Qt.Horizontal, "sort_id", QtCore.Qt.DisplayRole);
    dm.setHeaderData(7, QtCore.Qt.Horizontal, "manufacturer_id", QtCore.Qt.DisplayRole);
    dm.setHeaderData(8, QtCore.Qt.Horizontal, "model", QtCore.Qt.DisplayRole);
    dm.setHeaderData(9, QtCore.Qt.Horizontal, "description", QtCore.Qt.DisplayRole);
    dm.setHeaderData(10, QtCore.Qt.Horizontal, "group_id", QtCore.Qt.DisplayRole);
    dm.setHeaderData(11, QtCore.Qt.Horizontal, "need_approval", QtCore.Qt.DisplayRole);
    dm.setHeaderData(12, QtCore.Qt.Horizontal, "calibration_due_date", QtCore.Qt.DisplayRole);
    dm.setHeaderData(13, QtCore.Qt.Horizontal, "calibration_certificate_number", QtCore.Qt.DisplayRole);
    dm.setHeaderData(14, QtCore.Qt.Horizontal, "image_id", QtCore.Qt.DisplayRole);
    dm.setHeaderData(15, QtCore.Qt.Horizontal, "approver", QtCore.Qt.DisplayRole);
    dm.setHeaderData(16, QtCore.Qt.Horizontal, "garant", QtCore.Qt.DisplayRole);
    dm.setHeaderData(17, QtCore.Qt.Horizontal, "active", QtCore.Qt.DisplayRole);
    
    # now set the model into table view
    self.deviceView.setModel(dm)
    
  def showDeviceDetails(self, index=None):
    """Shows documents list about the selected device in Basic Info tab"""
    # get device_id for just selected row 
    model = self.deviceView.model()
    row = index.row()
    ind = model.index( row, 0)
    strDeviceId = model.data( ind)
    
    # set device reservation data
    rm = QtSql.QSqlTableModel()
    rm.setTable("device_documents");
    rm.setEditStrategy( QtSql.QSqlTableModel.EditStrategy.OnManualSubmit);
    rm.setFilter("device_id = %s" % str( strDeviceId))
    rm.select();
    
    docAbout = ""
    for i in range(rm.rowCount()):
      for j in range(rm.columnCount()):
        docAbout += str( rm.headerData( j,QtCore.Qt.Horizontal)) + " : "
        docAbout += str( rm.data( rm.index(i,j))) + "\t"
      docAbout += "\n"
    
    if not docAbout:
      docAbout = "No documents related to this device."
    self.reservationLabel.setText( docAbout)
  
  def showReservationDetails(self, index=None):
    """Shows reservation details on second tab of the information area"""
    # get device_id for just selected row 
    model = self.deviceView.model()
    row = index.row()
    ind = model.index( row, 0)
    strDeviceId = model.data( ind)
    
    # set device reservation data
    rm = QtSql.QSqlTableModel()
    rm.setTable("reservation");
    rm.setEditStrategy( QtSql.QSqlTableModel.EditStrategy.OnManualSubmit);
    rm.setFilter("devices_id = %s" % str( strDeviceId))
    rm.select();

    rm.setHeaderData(0, QtCore.Qt.Horizontal, "id", QtCore.Qt.DisplayRole);
    rm.setHeaderData(1, QtCore.Qt.Horizontal, "reservation_types_id", QtCore.Qt.DisplayRole);
    rm.setHeaderData(2, QtCore.Qt.Horizontal, "devices_id", QtCore.Qt.DisplayRole);
    rm.setHeaderData(3, QtCore.Qt.Horizontal, "category_id", QtCore.Qt.DisplayRole);
    rm.setHeaderData(4, QtCore.Qt.Horizontal, "from", QtCore.Qt.DisplayRole);
    rm.setHeaderData(5, QtCore.Qt.Horizontal, "to", QtCore.Qt.DisplayRole);
    rm.setHeaderData(6, QtCore.Qt.Horizontal, "status", QtCore.Qt.DisplayRole);
    rm.setHeaderData(7, QtCore.Qt.Horizontal, "eid", QtCore.Qt.DisplayRole);
    rm.setHeaderData(8, QtCore.Qt.Horizontal, "project", QtCore.Qt.DisplayRole);
    rm.setHeaderData(9, QtCore.Qt.Horizontal, "notes", QtCore.Qt.DisplayRole);
    rm.setHeaderData(10, QtCore.Qt.Horizontal, "stopedByReservation", QtCore.Qt.DisplayRole);
    rm.setHeaderData(11, QtCore.Qt.Horizontal, "insert_date", QtCore.Qt.DisplayRole);
    rm.setHeaderData(12, QtCore.Qt.Horizontal, "apdec_eid", QtCore.Qt.DisplayRole);
    rm.setHeaderData(13, QtCore.Qt.Horizontal, "apdec_date", QtCore.Qt.DisplayRole);
    rm.setHeaderData(14, QtCore.Qt.Horizontal, "apdec_text", QtCore.Qt.DisplayRole);
    
    # now set the model into table view
    self.reservationView.setModel(rm)
    
    
    

## THE EXECUTABLE
# ----------------------------------
# The main method to be run in case of running from the commandline
def main():
  loadSettings() # load the setting
  app = QtGui.QApplication(sys.argv)
  mainWindow = MainWindow()
  mainWindow.show()
  sys.exit(app.exec_())

if __name__ == '__main__':
  main()
