#!/bin/sh
# PLATFORM DEPENDENT INSTALLATION
# in Debian like OS (Ubuntu, Lubuntu, Raspbian, Mint, ...)

# as the root run:
sudo apt-get install python3-pyside git

# as the user run
cd ~
git clone git@bitbucket.org:koreniak/qtdbsearch.git
cd qtdbsearch
python3 main.py

# the default SQLite DB is used; you can change it in DBSettings dialog
